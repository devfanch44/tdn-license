<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Service\LicensePriceComputer;

class License extends Component
{
    public $license = 5;

    // Méthode 1 avec les Hooks

    // public $amount;

    // // Equivalent du contructeur
    // public function mount() {
    //     $this->amount = $this->computeAmount($this->license);
    // }

    // // Appeler après chaque mise à jour
    // public function updatedLicense(int $value): void {
    //     $this->amount = $this->computeAmount($value);
    // }

    // Méthode pas obligatoire car hérité
    public function render()
    {
        // Méthode 2 en passant une nouvelle prop au component
        return view('livewire.license', [
            'amount' => LicensePriceComputer::compute($this->license)
        ]);

        // On peut retourne directement du html code chaine de caractère si c'est un petit template (heredoc=" / nowdoc=')
        // return <<<'blade'
        //     <div class="text-center">
        //         <h1 class="text-secondary">Your license is currently for <strong>{{ $license }}</strong> people</h1>
        //         <input type="range" class="form-control-range" min="1" max="10" value="{{ $license }}">
        //         <h2 class="mt-5" style="color:grey;">Amount: ${{ $amount }}</h2>
        //     </div>
        // blade;
        // return view('livewire.license');
    }
}
