<?php

namespace App\Service;

class LicensePriceComputer
{
    public static function compute(int $license): float {
        return $license <= 5 ? $license * 15 : 50 + ($license - 5) * 10;
    }
}
