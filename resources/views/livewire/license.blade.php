<div class="text-center">
    <h1 class="text-secondary">
        Your license is currently for <strong>{{ pluralize($license, 'person', 'people') }}</strong>.
    </h1>
    <input type="range" wire:model="license" class="form-control-range" min="1" max="10" value="{{ $license }}">
    <h2 class="mt-5" style="color:grey;" >Amount: {{ money($amount) }}</h2>
</div>
